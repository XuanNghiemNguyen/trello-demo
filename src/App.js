import "./App.css";
import MainLayout from "./pages/todo";
import db from "./utils/database";
function App() {
	db.defaults({
		days: [
			{
				id: 0,
				name: "Monday",
			},
			{
				id: 1,
				name: "Tuesday",
			},
			{
				id: 2,
				name: "Wednesday",
			},
			{
				id: 3,
				name: "Thursday",
			},
			{
				id: 4,
				name: "Friday",
			},
			{
				id: 5,
				name: "Saturday",
			},
			{
				id: 6,
				name: "Sunday",
			},
		],
		types: [
			{
				id: "new-task",
				name: "New Task",
				dayId: 0,
				position: 0,
			},
			{
				id: "task-doing",
				name: "Doing",
				dayId: 0,

				position: 1,
			},
			{
				id: "task-done",
				name: "Done",
				dayId: 0,
				position: 2,
			},
		],
		tasks: [
			{
				id: "task-1",
				typeId: "new-task",
				position: 0,
				title: "title-example",
				content: "content-example",
			},
			{
				id: "task-2",
				typeId: "new-task",
				position: 1,
				title: "title-example",
				content: "content-example",
			},
			{
				id: "task-3",
				typeId: "new-task",
				position: 2,
				title: "title-example",
				content: "content-example",
			},
		],
	}).write();
	return (
		<div>
			<MainLayout />
		</div>
	);
}

export default App;
