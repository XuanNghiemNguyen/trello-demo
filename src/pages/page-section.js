import React from "react";
import TaskBlock from "./components/task-block";
import DragIndicatorOutlinedIcon from "@material-ui/icons/DragIndicatorOutlined";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import db from "../utils/database";
import "./index.css";
const SectionPage = (props) => {
	const { day } = props;
	const [sectionData, setSectionData] = React.useState(() => {
		return db.get("types").filter({ dayId: day }).sortBy("position").value();
	});

	React.useEffect(() => {
		setSectionData(
			db.get("types").filter({ dayId: day }).sortBy("position").value()
		);
	}, [day]);
	const handleDraggerEnd = ({ source, destination }) => {
		if (!source || !destination) return;
		const _source = {
			...db.get("types").find({ position: source.index }).value(),
		};
		db.get("types").remove({ position: source.index }).write();

		const _destination = {
			...db.get("types").find({ position: destination.index }).value(),
		};
		db.get("types").remove({ position: destination.index }).write();

		db.get("types")
			.push({ ..._source, position: destination.index })
			.write();
		db.get("types")
			.push({ ..._destination, position: source.index })
			.write();
		const _refetchData = db
			.get("types")
			.filter({ dayId: day })
			.sortBy("position")
			.value();
		setSectionData(_refetchData);
	};
	return (
		<DragDropContext onDragEnd={handleDraggerEnd}>
			<Droppable droppableId={`droppable-task	`} direction='horizontal'>
				{(provided) => (
					<div
						style={{
							display: "flex",
							flexDirection: "row",
							justifyContent: "space-around",
							padding: 3,
						}}
						{...provided.droppableProps}
						ref={provided.innerRef}>
						{sectionData?.map((type, index) => (
							<Draggable
								key={`key-${type.id}`}
								draggableId={`dragger-${type.id}`}
								index={type.position}>
								{(_provided) => (
									<div
										ref={_provided.innerRef}
										{..._provided.draggableProps}
										className='task-block'>
										<span {..._provided.dragHandleProps}>
											<DragIndicatorOutlinedIcon
												style={{
													position: "absolute",
													cursor: "grab",
													right: 4,
													top: 4,
													fontSize: 20,
												}}
											/>
										</span>
										<TaskBlock type={type}></TaskBlock>
									</div>
								)}
							</Draggable>
						))}
						{provided.placeholder}
					</div>
				)}
			</Droppable>
		</DragDropContext>
	);
};

export default SectionPage;
