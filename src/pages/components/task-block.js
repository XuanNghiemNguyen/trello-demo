import { Paper } from "@material-ui/core";
import React from "react";
import Task from "./task";
import DragIndicatorOutlinedIcon from "@material-ui/icons/DragIndicatorOutlined";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import db from "../../utils/database";
import "../index.css";
const TaskBlock = (props) => {
	const { type } = props;
	// const [taskOnBlockData, setTaskOnBlockData] = React.useState(() => {
	// 	return db
	// 		.get("tasks")
	// 		.filter({ typeId: type.id })
	// 		.sortBy("position")
	// 		.value();
	// });
	const taskOnBlockData = db
		.get("tasks")
		.filter({ typeId: type.id })
		.sortBy("position")
		.value();
	console.log(taskOnBlockData);
	return (
		<Paper elevation={3}>
			<div style={{ margin: 8 }}>
				<b>{props.type.name || "New Task"}</b>
			</div>
			<DragDropContext onDragEnd={() => {}}>
				<Droppable droppableId={`droppable-task`}>
					{(provided) => (
						<div
							style={{
								display: "flex",
								flexDirection: "column",
								justifyContent: "flex-start",
								width: "100%",
							}}
							{...provided.droppableProps}
							ref={provided.innerRef}>
							{[...taskOnBlockData]?.map((task, index) => (
								<Draggable
									key={`key-task-${task.id}`}
									draggableId={`dragger-task-${task.id}`}
									index={task.position}>
									{(_provided) => (
										<div
											ref={_provided.innerRef}
											{..._provided.draggableProps}
											className='task'>
											<span {..._provided.dragHandleProps}>
												<DragIndicatorOutlinedIcon
													style={{
														position: "absolute",
														cursor: "pointer",
														right: 5,
														top: 6,
														fontSize: 16,
													}}
												/>
											</span>
											<Task key={index} task={task} />
										</div>
									)}
								</Draggable>
							))}
							{provided.placeholder}
						</div>
					)}
				</Droppable>
			</DragDropContext>
		</Paper>
	);
};
export default TaskBlock;
