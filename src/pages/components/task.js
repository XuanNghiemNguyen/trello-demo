import { Paper } from "@material-ui/core";
import React from "react";

const Task = (props) => {
	const { task } = props;
	return (
		<Paper elevation={4} style={{ margin: 5, padding: 5 }}>
			{task?.title || "task?.title"}
		</Paper>
	);
};
export default Task;
