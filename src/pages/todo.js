import { Divider, Paper, Tab, Tabs } from "@material-ui/core";
import React from "react";
import SectionPage from "./page-section";
import db from "../utils/database";

const _days = db.get("days").value();
const MainLayout = () => {
	const [tabActive, setTabActive] = React.useState(0);
	const handleTabChange = (event, value) => {
		setTabActive(value);
	};
	return (
		<>
			<div
				style={{
					display: "flex",
					flexDirection: "row",
					backgroundImage: `url("https://i.pinimg.com/originals/cb/c2/2c/cbc22ca5a3d7568a742262639a9f6b3f.jpg`,
					justifyContent: "center",
				}}>
				<Paper
					square
					style={{
						display: "flex",
						flexDirection: "column",
						height: "94vh",
						marginTop: "3vh",
						marginBottom: "3vh",
						borderRadius: 5,
					}}>
					<Tabs
						style={{ marginBottom: 20 }}
						value={tabActive}
						indicatorColor='primary'
						textColor='primary'
						onChange={handleTabChange}
						aria-label='disabled tabs example'>
						{_days?.map((date) => (
							<Tab key={date.id} label={date.name} />
						))}
					</Tabs>
					<Divider />
					<SectionPage day={tabActive} />
				</Paper>
			</div>
		</>
	);
};
export default MainLayout;
